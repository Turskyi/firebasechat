package ua.turskyi.firebasechat

/**
 * @Description Constants for the app.
 */
object Constants {
    const val RESULT_CODE_SIGN_IN = 1
}